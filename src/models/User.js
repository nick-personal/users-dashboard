export default class User {
	
	constructor(userData) {
		Object.assign(this, userData);
	}
	
	get url() {
		return `/${this.id}`;
	}
	
	get fullAddress() {
		return `${this.address.suite} ${this.address.street}, ${this.address.city}, ${this.address.zipcode}`;
	}
	
	get imageAltText() {
		return `${this.name}'s avatar.`;
	}
	
}
