import Vue from 'vue';
import App from '@/App.vue';
import router from '@/router';

Vue.config.productionTip = false;

// Dump function, so console.log would only print in development mode
window.dump = (...args) => {
	if (process.env.NODE_ENV === 'development') {
		console.log(args);
	}
};

new Vue({
	router,
	render: (h) => h(App),
}).$mount('#app');
