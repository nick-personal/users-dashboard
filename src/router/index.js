import Vue from 'vue';
import VueRouter from 'vue-router';
import Users from '@/views/Users';
import UserDetails from '@/views/UserDetails';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'Users',
		component: Users,
	},
	{
		path: '/:id',
		name: 'UserDetails',
		component: UserDetails,
	},
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

export default router;
